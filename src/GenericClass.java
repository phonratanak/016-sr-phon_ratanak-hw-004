public class GenericClass<T> {
    private Object array[];
    private int count;
    public int size;
    public GenericClass() {
        array = new Object[1];
        count = 0;
        size = 1;
    }
    public void addItems(T data) throws DuplicateNames {
        if (data == null)
        {
            throw new NullPointerException("Inputted  null value!");
        }
        else
        {
            for (int i=0;i<size;i++)
                if(array[i]==(T)data){
                    throw new DuplicateNames("Duplication value: "+data);
                }
            if (count == size) {
                getSize();
            }
            array[count] = data;
            count++;
        }
    }
    public void getSize() {
        Object tmp[] = null;
        if (count == size) {
            tmp = new Object[size * 2];
            {
                for (int i = 0; i < size; i++) {
                    tmp[i] = array[i];
                }
            }
        }
        array = tmp;
        size = size * 2;
    }
    public void calculateSize() {
        Object tmp[] = null;
        if (count > 0) {
            tmp = new Object[count];
            for (int i = 0; i < count; i++) {
                tmp[i] = array[i];
            }
            size = count;
            array = tmp;
        }
    }
    public Object getValue(int i)
    {
        return this.array[i];
    }
}
